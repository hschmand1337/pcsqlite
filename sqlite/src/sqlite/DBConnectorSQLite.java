package sqlite;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DBConnectorSQLite {
    
    private Connection conn = null;
    private String dbDriver = "jdbc:sqlite";
    private String dbPath = "/Users/mac/Desktop/ITS/SE/_gitlab/pcsqlite/";

    /**
     * Connect to sample DB
     * @param dbName
     * @return
     */
    public Connection connect(String dbName) {
        //connection string
        String connString = dbDriver + ":" + dbPath + dbName + ".db";
        
        try {
            //open a connection to the DB
            conn = DriverManager.getConnection(connString);
            
        } catch (SQLException ex) {
            Logger.getLogger(DBConnectorSQLite.class.getName()).log(Level.SEVERE, null, ex);
        }
        return conn;
    }
    
    //public void 
}