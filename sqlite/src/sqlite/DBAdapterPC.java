package sqlite;

import java.awt.List;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DBAdapterPC implements DataInterfacePC {

    DBConnectorSQLite dbConnector = new DBConnectorSQLite();
    Connection conn;
    private ArrayList<PC> pcListe;
    
    DBAdapterPC(String dbname) {
        conn = dbConnector.connect(dbname);
    }
    
    @Override
    public void insertPC(PC pc) {
        String sql = "INSERT INTO pc VALUES(?, ?, ?, ?)";
        
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, pc.hostname);
            ps.setInt(2, pc.ram);
            ps.setDouble(3, pc.takt);
            ps.setDouble(4, pc.hdd);
            ps.execute();
            ps.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    @Override
    public void updatePC(PC pc) {
        String sql = "UPDATE pc SET ram = ?, takt = ?, hdd = ? WHERE hostname = ?";
        
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, pc.ram);
            ps.setDouble(2, pc.takt);
            ps.setDouble(3, pc.hdd);
            ps.setString(4, pc.hostname);
            ps.execute();
            ps.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    @Override
    public boolean deletePC(PC pc) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ArrayList<PC> getAllPCs() {
        pcListe = new ArrayList<PC>();
        String sql = "SELECT * FROM pc";
        
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            
            while(rs.next()) {
                PC pc = new PC();
                pc.hostname = rs.getString("hostname");
                pc.ram = rs.getInt("ram");
                pc.takt = rs.getDouble("takt");
                pc.hdd = rs.getDouble("hdd");
                pcListe.add(pc);
            }
            rs.close();
            ps.close();
            return pcListe;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }
    
    @Override
    public boolean getPC(PC pc) {
        return true;
    }
}
