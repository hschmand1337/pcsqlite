package sqlite;

public class PC {
    public String hostname = "";
    public int ram = 0;
    public double takt = 0.0;
    public double hdd = 0.0;
    
    PC() {
        
    }
    
    PC(String hostname, int ram, double takt, double hdd) {
        this.hostname = hostname;
        this.ram = ram;
        this.takt = takt;
        this.hdd = hdd;
    }
}
