package sqlite;

import java.sql.Connection;
import java.util.ArrayList;

public interface DataInterfacePC {

    /**
     *
     */
   
    public void insertPC(PC pc);
    
    public void updatePC(PC pc);
    
    public boolean deletePC(PC pc);
    
    public ArrayList<PC> getAllPCs();
    
    public boolean getPC(PC pc);
}
